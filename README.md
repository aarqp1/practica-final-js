# Práctica final curso Js

## En el presente repo se colocarán las carpetas correpondientes al ejercicio final.

En el se desarrolla un proyecto de js, en el que se aplican los temas vistos, tales como: JQuery, Ajax, DOM y JSON para conectar una aplicación web a un API delsitio web themoviedb.org con la finalidad de obtener datos y mostrarlos en la app.

## Solución del ejercicio

El caso busca realizar una implementación de la API en una barra de búsqueda, que permita buscar dentro de los datos de una API una palabra determianda para quearroje resultados que contengan dicha palabra.

Para resolver el ejercicio se identificó que en el docuemento .js correspondiente se debe crear una función que recibe como parámetro un objeto deun archivo JSON y permita crear la estructura HTML en la que se publicarán los resultados obtenidos y retornando un código HTML con los datos de cada objeto película.

También, se debe crear un JQuery en la que se coloca "document" que es un elemento de HTML DOM y la accción ready que permite que se ejecute el JQuery en el momento que la página se ha cargado.

Además se requiere de una implementación de ajax con $.ajasx que procede a realzair una petición a la url de la API, en la que pasamos la palabra que el usuario a colocado en la barra de búsqueda para que nos retorne objetos en un archivo JJSON que se nos desplegará la información obtenida y su estructura en HTML con la función previamente definida.


### Autor

* Aarón Quintanar
